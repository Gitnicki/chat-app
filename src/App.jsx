import React, { useState, useRef, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

function ChatApp() {
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const messagesEndRef = useRef(null);

  const handleSendMessage = () => {
    if (newMessage.trim()!== '') {
      const newMessageObject = { id: uuidv4(), text: newMessage, timestamp: Date.now() };
      setMessages([...messages, newMessageObject]);
      setNewMessage('');
    } else {
      alert('Please enter a message!');
    }
  };

  useEffect(() => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  }, [messages]);

  const clearChatHistory = () => {
    setMessages([]);
  };

  return (
    <div>
      <h1>Chat App</h1>
      <ul>
        {messages.map((message) => (
          <li key={message.id}>{message.text} ({new Date(message.timestamp).toLocaleTimeString()})</li>
        ))}
        <div ref={messagesEndRef} />
      </ul>
      <input
        type="text"
        value={newMessage}
        onChange={(e) => setNewMessage(e.target.value)}
        placeholder="Type a message..."
      />
      <button onClick={handleSendMessage}>Send</button>
      <button onClick={clearChatHistory}>Clear Chat</button>
    </div>
  );
}

export default ChatApp;